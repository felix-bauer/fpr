module App

open Elmish
open Elmish.React
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma

module Browser = Fable.Import.Browser

// MODEL

[<AbstractClass; Sealed>]
type IdGenerator private() =
    static let mutable id : int = 0
    static member getNextId : int = 
        id <- id + 1
        id
        
type ToDoStatus =
    | NewTodo
    | FinishedTodo

type Todo(title : string) =
    member val id = IdGenerator.getNextId
    member val title = title
    member val status = ToDoStatus.NewTodo with get,set

    member this.Equals(todo : Todo) : bool =
        todo.id = this.id

type Model = {
    ToDoForm: string
    ToDos: Todo list
}

type Message =
    | UpdateForm of string
    | CreateTodo
    | FinishTodo of Todo
    | DeleteTodo of Todo

let init() : Model = {
    ToDoForm = ""
    ToDos = []
}

// UPDATE

let helperGetStatusAsString (todo : Todo) : string =
    match todo.status with
        | NewTodo -> "New Todo"
        | FinishedTodo -> "Done Todo"

let finish (todo: Todo) : unit =
    match todo.status with
    | NewTodo -> todo.status <- ToDoStatus.FinishedTodo
    | FinishedTodo ->  todo.status <- ToDoStatus.NewTodo

let doIfEqual (todo: Todo) (compare: Todo) (f: Todo -> Todo): Todo =
    match todo.Equals compare with
    | true -> f todo
    | _ -> todo

let update (message: Message) (model: Model): Model =
    match message with
    | UpdateForm content ->
        { model with ToDoForm = content }
    | CreateTodo ->
        if (model.ToDoForm.Length > 0) then
            let newDraft = Todo(model.ToDoForm)
            { model with
                ToDoForm = ""
                ToDos = newDraft::model.ToDos }
        else
            model
    | FinishTodo todo ->
        finish todo
        { model with ToDos = model.ToDos }
    | DeleteTodo todo ->
        let todos =
            model.ToDos
            |> List.filter (fun x -> not (todo.Equals x))
        { model with ToDos = todos }

// VIEW (rendered with React)

open Fulma
open Fable.Import

let newTodoTile dispatch (todo: Todo) =
    Tile.tile [ Tile.IsChild; Tile.Size Tile.Is4; Tile.CustomClass "content-card" ]
        [ Card.card [ ]
            [ Card.header []
                [ Card.Header.title [] [ str (todo.title) ]]
              Card.footer []
                [ Card.Footer.div [ GenericOption.Props [ OnClick (fun _ -> FinishTodo todo |> dispatch) ] ]
                     [ Icon.icon [ Icon.Size IsSmall ]
                        [ i [ ClassName "fa fa-check" ] [] ]
                     ]
                  Card.Footer.div [ GenericOption.Props [ OnClick (fun _ -> DeleteTodo todo |> dispatch) ] ]
                    [ Icon.icon [ Icon.Size IsSmall ]
                        [ i [ ClassName "fa fa-trash" ] [] ]
                    ]
                ]
            ]
        ]

let finishedTodoTile dispatch (todo: Todo) =
    Tile.tile [ Tile.IsChild; Tile.Size Tile.Is4; Tile.CustomClass "content-card" ]
        [ Card.card [ ]
            [ Card.header []
                [ Card.Header.title [] [ span [ Class "striketrough" ] [ str (todo.title) ] ] ]
              Card.footer []
                [ Card.Footer.div [ GenericOption.Props [ OnClick (fun _ -> FinishTodo todo |> dispatch) ] ]
                     [ Icon.icon [ Icon.Size IsSmall ]
                        [ i [ ClassName "fa fa-undo" ] [] ]
                     ]
                  Card.Footer.div [ GenericOption.Props [ OnClick (fun _ -> DeleteTodo todo |> dispatch) ] ]
                    [ Icon.icon [ Icon.Size IsSmall ]
                        [ i [ ClassName "fa fa-trash" ] [] ]
                    ]
                ]
            ]
        ]

let toCard dispatch (todo: Todo) =
    match todo.status with
    | NewTodo _ ->
        newTodoTile dispatch todo
    | FinishedTodo ->
        finishedTodoTile dispatch todo

let toCardRow row =
    Tile.tile [ Tile.IsParent; Tile.Size Tile.Is12 ] row

let rec chunkByThree soFar l =
    match l with
    | x1::x2::[x3] ->
        [x1; x2; x3]::soFar
    | x1::x2::x3::xs ->
        chunkByThree ([x1; x2; x3]::soFar) xs
    | xs ->
        xs::soFar

let toCardRows dispatch (titles: Todo list) =
    titles
    |> chunkByThree []
    |> List.rev
    |> List.map ((List.map (toCard dispatch)) >> toCardRow)

let view (model: Model) dispatch =   
    div []
      [ Navbar.navbar [ Navbar.Color IsBlack ]
            [ Navbar.Brand.div []
                [ Navbar.Item.a [ Navbar.Item.Props [ Href "#" ] ]
                    [ str "Todo Manager" ] ] ]
        Container.container [ Container.IsFluid ]
          [ h1 [ Class "is-size-1 app-title" ] [ str "Manage your Todos" ]
            Tile.tile [ Tile.IsAncestor; Tile.IsVertical ]
                [ yield Tile.tile [ Tile.IsParent; Tile.Size Tile.Is12 ]
                    [ Tile.tile [ Tile.IsChild ]
                        [ Card.card []
                            [ Card.header []
                                [ Card.Header.title [] [ str "Add a Todo" ] ]
                              Card.content []
                                [ Input.text [ Input.Placeholder "New Todo"
                                               Input.Value model.ToDoForm
                                               Input.OnChange (fun ev -> UpdateForm ev.Value |> dispatch)
                                               Input.Option.Props
                                                 [ OnKeyUp (fun ev -> if ev.key = "Enter" then dispatch CreateTodo) ] ] ]
                              Card.footer []
                                [ Card.Footer.a [ GenericOption.Props [ OnClick (fun _ -> dispatch CreateTodo) ] ]
                                    [ str "Add" ] ] ] ] ]
                  yield! model.ToDos |> toCardRows dispatch ] ] ]

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

// App
Program.mkSimple init update view
|> Program.withReactUnoptimized "elmish-app"
#if DEBUG
// |> Program.withConsoleTrace
|> Program.withDebugger
#endif
|> Program.run
